import data.items.Items;
import data.items.armors.Armor;
import data.items.armors.PowerArmorCalculator;
import data.items.armors.boots.Boots;
import data.items.armors.shields.Shield;
import data.items.TypeOfItem;
import data.items.weapions.swords.Sword;
import data.players.Player;
import interfaces.*;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        MainGameInterface mainGameInterface = new MainGameInterface();


        PowerArmorCalculator powerArmorCalculator = new PowerArmorCalculator();
        ArrayList<Armor> playerArmorSet = new ArrayList<>();

        ArrayList<Items> goblinLoot = new ArrayList<>();
        goblinLoot.add( new Boots(TypeOfItem.RARE,1,100));
        goblinLoot.add(new Shield(TypeOfItem.RARE, 8,100));
        goblinLoot.add(new Sword(TypeOfItem.COMMON,1,100 ));




        Player player10 = new Player("fcgh", 1000, 100, playerArmorSet,100,100  );




        player10.addAllToInventory(goblinLoot);

        player10.putOnArmor(1);
        for (Armor armor : playerArmorSet) {
            armor.setPowerOfArmor(powerArmorCalculator.calculate(armor.getDefaultArmor(), armor));


        }
        player10.setPlayerArmorValue(playerArmorSet);



        System.out.println(player10.toString());



    }
}
