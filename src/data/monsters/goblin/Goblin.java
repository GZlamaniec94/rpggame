package data.monsters.goblin;

import data.items.Items;
import data.items.TypeOfItem;
import data.items.armors.body.Body;
import data.monsters.Monster;

import java.util.ArrayList;
import java.util.Random;

public class Goblin extends Monster {



    public Goblin (int monsterHealth, int experienceGiven, int monsterArmorValue, int monsterAttackDamage, String monsterName, ArrayList<Items> goblinLoot){
        this.monsterHealth=monsterHealth;
        this.experienceGiven=experienceGiven;
        this.monsterArmorValue=monsterArmorValue;
        this.monsterAttackDamage=monsterAttackDamage;
        this.monsterName=monsterName;
        this.monsterLoot=goblinLoot;
    }






}
