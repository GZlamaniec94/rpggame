package data.items;

/**
 * There are three types of data.items. If more rare is the item, the better bonus it has.
 */
public enum TypeOfItem {
    COMMON(0),
    RARE(15),
    UNIQUE(30);

    private final Integer TYPE;

    TypeOfItem(int type) {
        this.TYPE = type;
    }

    public Integer getTYPE() {
        return TYPE;
    }
}
