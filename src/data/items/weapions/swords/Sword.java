package data.items.weapions.swords;

import data.items.TypeOfItem;
import data.items.weapions.Weapon;

public class Sword extends Weapon {

    public Sword(TypeOfItem typeOfItem, int levelOfUpgrade, int poweOfAttack){
        this.typeOfItem=typeOfItem;
        this.powerOfAttack=poweOfAttack;
        this.levelOfUpgrade=levelOfUpgrade;



    }

    @Override
    public String toString() {
        return "Sword{" +
                "typeOfItem=" + typeOfItem +
                ", powerOfAttack=" + powerOfAttack +
                ", levelOfUpgrade=" + levelOfUpgrade +
                '}';
    }
}
