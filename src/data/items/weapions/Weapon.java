package data.items.weapions;

import data.items.Items;
import data.items.TypeOfItem;

public abstract class Weapon extends Items {

    protected TypeOfItem typeOfItem;
    protected int powerOfAttack;
    protected int levelOfUpgrade;



    public void setPowerOfAttack(int powerOfAttack) {
        this.powerOfAttack = powerOfAttack;
    }



    public TypeOfItem getTypeOfItem() {
        return typeOfItem;
    }

    public int getPowerOfAttack() {
        return powerOfAttack;
    }

    public int getLevelOfUpgrade() {
        return levelOfUpgrade;
    }







}
