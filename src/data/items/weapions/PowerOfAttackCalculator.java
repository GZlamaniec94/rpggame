package data.items.weapions;

import data.items.TypeOfItem;

import static data.items.TypeOfItem.RARE;
import static data.items.TypeOfItem.UNIQUE;

public class PowerOfAttackCalculator {

        public int calculate(int defaultPowerOfAttack, Weapon weapion) {
            switch (weapion.getTypeOfItem()) {
                case RARE:
                    int armorFromType = RARE.getTYPE();
                    return (defaultPowerOfAttack + ((defaultPowerOfAttack * armorFromType) / 100)) + ((defaultPowerOfAttack * (5 * weapion.getLevelOfUpgrade())) / 100);
                case UNIQUE:
                    armorFromType = UNIQUE.getTYPE();
                    return (defaultPowerOfAttack + ((defaultPowerOfAttack * armorFromType) / 100)) + ((defaultPowerOfAttack * (5 * weapion.getLevelOfUpgrade())) / 100);
                default:
                    armorFromType = TypeOfItem.COMMON.getTYPE();
                    return (defaultPowerOfAttack + ((defaultPowerOfAttack * armorFromType) / 100)) + ((defaultPowerOfAttack * (5 * weapion.getLevelOfUpgrade())) / 100);
            }
        }
    }


