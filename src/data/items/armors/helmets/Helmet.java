package data.items.armors.helmets;

import data.items.armors.Armor;
import data.items.TypeOfItem;

public class Helmet extends Armor {

    public Helmet (TypeOfItem typeOfItem, int levelOfUpgrade, int defaultArmor) {
        this.typeOfItem =typeOfItem;
        this.levelOfUpgrade = levelOfUpgrade;
        this.defaultArmor=defaultArmor;


    }


    @Override
    public String toString() {
        return "Helmet{" +
                "typeOfArmor=" + typeOfItem +
                ", powerOfArmor=" + powerOfArmor +
                ", levelOfUpgrade=" + levelOfUpgrade +
                '}';
    }



}
