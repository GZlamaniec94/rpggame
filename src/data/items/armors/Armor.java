package data.items.armors;
import data.items.Items;
import data.items.TypeOfItem;

public abstract class Armor extends Items implements Armors {

    protected TypeOfItem typeOfItem;
    protected int powerOfArmor;
    protected int levelOfUpgrade;
    protected int defaultArmor;



    public int getDefaultArmor() {
        return defaultArmor;
    }

    public void setDefaultArmor(int defaultArmor) {
        this.defaultArmor = defaultArmor;
    }


    public TypeOfItem getTypeOfArmor() {
        return typeOfItem;
    }

    public int getLevelOfUpgrade() {
        return levelOfUpgrade;
    }

    public void setPowerOfArmor(int powerOfArmor) {
        this.powerOfArmor = powerOfArmor;
    }

    @Override
    public int getPowerOfArmor() {
        return this.powerOfArmor;
    }
}

