package data.items.armors.body;

import data.items.armors.Armor;
import data.items.TypeOfItem;

public class Body extends Armor {


    public Body (TypeOfItem typeOfItem, int levelOfUpgrade, int defaultArmor) {
        this.typeOfItem =typeOfItem;
        this.levelOfUpgrade = levelOfUpgrade;
        this.defaultArmor=defaultArmor;


    }


    @Override
    public String toString() {
        return "Body{" +
                "typeOfArmor=" + typeOfItem +
                ", powerOfArmor=" + powerOfArmor +
                ", levelOfUpgrade=" + levelOfUpgrade +
                '}';
    }


}
