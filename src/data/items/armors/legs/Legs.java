package data.items.armors.legs;

import data.items.armors.Armor;
import data.items.TypeOfItem;
public class Legs extends Armor {

    public Legs (TypeOfItem typeOfItem, int levelOfUpgrade, int defaultArmor) {
        this.typeOfItem =typeOfItem;
        this.levelOfUpgrade = levelOfUpgrade;
        this.defaultArmor=defaultArmor;


    }

    @Override
    public String toString() {
        return "Legs{" +
                "typeOfArmor=" + typeOfItem +
                ", powerOfArmor=" + powerOfArmor +
                ", levelOfUpgrade=" + levelOfUpgrade +
                '}';
    }


}
