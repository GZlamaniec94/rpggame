package data.items.armors.boots;

import data.items.armors.Armor;
import data.items.TypeOfItem;

public class Boots extends Armor {

    public Boots (TypeOfItem typeOfItem, int levelOfUpgrade, int defaultArmor) {
        this.typeOfItem =typeOfItem;
        this.levelOfUpgrade = levelOfUpgrade;
        this.defaultArmor=defaultArmor;


    }

    @Override
    public String toString() {
        return "Boots{" +
                "typeOfArmor=" + typeOfItem +
                ", powerOfArmor=" + powerOfArmor +
                ", levelOfUpgrade=" + levelOfUpgrade +
                '}';
    }

}
