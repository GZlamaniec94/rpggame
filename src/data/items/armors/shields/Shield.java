package data.items.armors.shields;

import data.items.armors.Armor;
import data.items.TypeOfItem;


public class Shield extends Armor {

    public Shield (TypeOfItem typeOfItem, int levelOfUpgrade, int defaultArmor) {
        this.typeOfItem =typeOfItem;
        this.levelOfUpgrade = levelOfUpgrade;
        this.defaultArmor=defaultArmor;


    }


    @Override
    public String toString() {
        return "Shield{" +
                "typeOfArmor=" + typeOfItem +
                ", powerOfArmor=" + powerOfArmor +
                ", levelOfUpgrade=" + levelOfUpgrade +
                '}';
    }
}
