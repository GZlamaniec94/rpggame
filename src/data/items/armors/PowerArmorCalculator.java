package data.items.armors;

import data.items.TypeOfItem;

import static data.items.TypeOfItem.RARE;
import static data.items.TypeOfItem.UNIQUE;

public class PowerArmorCalculator {

    public int calculate(int defaultArmor, Armor armor) {
        switch (armor.getTypeOfArmor()) {
            case RARE:
                int armorFromType = RARE.getTYPE();
                return (defaultArmor + ((defaultArmor * armorFromType) / 100)) + ((defaultArmor * (5 * armor.getLevelOfUpgrade())) / 100);
            case UNIQUE:
                armorFromType = UNIQUE.getTYPE();
                return (defaultArmor + ((defaultArmor * armorFromType) / 100)) + ((defaultArmor * (5 * armor.getLevelOfUpgrade())) / 100);
            default:
                armorFromType =TypeOfItem.COMMON.getTYPE();
                return (defaultArmor + ((defaultArmor * armorFromType) / 100)) + ((defaultArmor * (5 * armor.getLevelOfUpgrade())) / 100);
        }
    }
}
