package data.players;

import data.items.Items;
import data.items.armors.Armor;

import java.util.ArrayList;

public class Player {

    private String name;

    private int playerHealth;
    private int playerStrength;

    private int playerAttackValue;
    private int playerArmorValue;

    private ArrayList<Armor> playerArmorSet;
    private ArrayList<Items> playerInventory=new ArrayList<>();


    public Player(String name, int health, int playerArmorValue, ArrayList<Armor> playerArmorSet, int playerStrength, int playerAttackValue) {
        this.name = name;
        this.playerHealth = health;
        this.playerArmorValue = playerArmorValue;
        this.playerArmorSet = playerArmorSet;
        this.playerStrength=playerStrength;
        this.playerAttackValue=playerAttackValue;



    }


    public void addToInventory(ArrayList<Items> otherInventory, int index){
      this.playerInventory.add(otherInventory.get(index-1));
      otherInventory.remove(index-1);


    }

    public void addAllToInventory (ArrayList<Items> otherInventory){
        this.playerInventory.addAll(otherInventory);
        otherInventory.removeAll(playerInventory);
    }

    public void putOnArmor ( int index){
        this.playerArmorSet.add((Armor) playerInventory.get(index-1));
        playerInventory.remove(index-1);

    }

    public void setPlayerArmorValue(ArrayList<Armor> playerArmorSet) {
        for (Armor armor : playerArmorSet) {
            int armorValueFromArmor;
            armorValueFromArmor = armor.getPowerOfArmor();
            this.playerArmorValue += armorValueFromArmor;

        }
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", playerHealth=" + playerHealth +
                ", playerStrength=" + playerStrength +
                ", playerAttackValue=" + playerAttackValue +
                ", playerArmorValue=" + playerArmorValue +
                ", playerArmorSet=" + playerArmorSet +
                ", playerInventory=" + playerInventory +
                '}';
    }
}
